Source: cht.sh
Section: misc
Priority: optional
Maintainer: Thiago Marques Siqueira <thiagoms.15@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://cheat.sh
Vcs-Browser: https://salsa.debian.org/debian/cht.sh
Vcs-Git: https://salsa.debian.org/debian/cht.sh.git

Package: cht.sh
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}, curl, rlwrap, xsel
Description: Cht is the only cheat sheet you need
 The cheat.sh service has its own command line client (cht.sh) that has several
 useful features compared to querying the service directly with curl:
   - Special shell mode with a persistent queries context and readline support.
   - Queries history.
   - Clipboard integration.
   - Tab completion support for shells (bash, fish, zsh).
   - Stealth mode.
 .
 The cheat.sh service:
   - Covers 56 programming languages, several DBMSes, and more than 1000 most
     important UNIX/Linux commands.
   - Provides access to the best community driven cheat sheets repositories
     in the world, on par with StackOverflow.
